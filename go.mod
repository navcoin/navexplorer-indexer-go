module github.com/navcoin/navexplorer-indexer-go/v2

go 1.14

require (
	github.com/certifi/gocertifi v0.0.0-20200922220541-2c3bb06c6054 // indirect
	github.com/getsentry/raven-go v0.2.0
	github.com/gosimple/slug v1.9.0
	github.com/joho/godotenv v1.3.0
	github.com/mattn/go-colorable v0.1.8
	github.com/navcoin/navcoind-go v1.1.7
	github.com/navcoin/subscriber v1.0.3
	github.com/olivere/elastic/v7 v7.0.21
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/sarulabs/di/v2 v2.4.2
	github.com/sarulabs/dingo/v4 v4.2.0
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/cobra v1.1.1 // indirect
	github.com/streadway/amqp v1.0.0 // indirect
	go.uber.org/zap v1.16.0
)
